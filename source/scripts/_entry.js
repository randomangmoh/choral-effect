import Wad from 'web-audio-daw';
import Tuna from 'tunajs';

// Entry Point
document.addEventListener('DOMContentLoaded', () => {


    /**
     * HTML Elements
     */
    const button = document.querySelector('[data-app="trigger"]');

    const delayWrapper = document.querySelector('[data-app="delay"]');
    const delay = delayWrapper.querySelector('[data-app="input"]');
    const delayLabel = delayWrapper.querySelector('[data-app="input-amount"]');

    const volumeWrapper = document.querySelector('[data-app="volume"]');
    const volume = volumeWrapper.querySelector('[data-app="input"]');
    const volumeLabel = volumeWrapper.querySelector('[data-app="input-amount"]');


    /**
     * Global values
     */
    let IS_STREAMING = false;
    let CURRENT_DELAY = parseInt(delay.value) / 100;
    let CURRENT_VOLUME = parseInt(volume.value) / 100;

    const BUTTON_COPY_PLAY = 'Stop';
    const BUTTON_COPY_STOP = 'Start';


    /**
     * Stream WAD
     */
    let streamInput = new Wad({
        source: 'mic',
        volume: 0.7,
        delay: {
            wet: 1,
            delayTime: CURRENT_DELAY,
            feedback: 0
        },
        compressor : {
            attack    : .003, // The amount of time, in seconds, to reduce the gain by 10dB. This parameter ranges from 0 to 1.
            knee      : 30,   // A decibel value representing the range above the threshold where the curve smoothly transitions to the "ratio" portion. This parameter ranges from 0 to 40.
            ratio     : 12,  // The amount of dB change in input for a 1 dB change in output. This parameter ranges from 1 to 20.
            release   : .8, // The amount of time (in seconds) to increase the gain by 10dB. This parameter ranges from 0 to 1.
            threshold : -24, // The decibel value above which the compression will start taking effect. This parameter ranges from -100 to 0.
        }
    });

    let stream;

    /**
     * Event Listeners
     */

    button.addEventListener('click', (e) => {
        if(!IS_STREAMING) startStream();
            else stopStream();
    });

    delay.addEventListener('input', (e) => onDelayChange(e));
    volume.addEventListener('input', (e) => onVolumeChange(e));


    /**
     * On delay change, update input node
     * @param {Object} e 
     */
    function onDelayChange(e) {

        let value = parseInt(e.currentTarget.value) / 100;
        CURRENT_DELAY = value;
        streamInput.setDelay(value, 1, 0);

        delayLabel.innerHTML = value;

    }


    /**
     * On volume change, update input node
     * @param {Object} e 
     */
    function onVolumeChange(e) {

        let value = parseInt(e.target.value) / 100;
        let roundedValue = Math.round(value * 10) / 10;
        
        CURRENT_VOLUME = roundedValue;

        volumeLabel.innerHTML = roundedValue * 100 + '%';

    }

    function startStream() {

        IS_STREAMING = true;
        streamInput.play();

        button.innerHTML = BUTTON_COPY_PLAY;
        
        if(!IS_STREAMING) stream = requestAnimationFrame(updateStream);
            else stream = null;

    }

    function stopStream() {

        IS_STREAMING = false;
        streamInput.stop();

        window.cancelAnimationFrame(stream);

        button.innerHTML = BUTTON_COPY_STOP;

    }

    function updateStream(volume, delay) {

        console.log(streamInput.mediaStreamSource);

        requestAnimationFrame(updateStream);

    }


});

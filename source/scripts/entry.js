import Pizzicato from 'pizzicato';

// Entry Point
document.addEventListener('DOMContentLoaded', () => {


    /**
     * HTML Elements
     */
    const button = document.querySelector('[data-app="trigger"]');

    const delayWrapper = document.querySelector('[data-app="delay"]');
    const delay = delayWrapper.querySelector('[data-app="input"]');
    const delayLabel = delayWrapper.querySelector('[data-app="input-amount"]');

    const volumeWrapper = document.querySelector('[data-app="volume"]');
    const volume = volumeWrapper.querySelector('[data-app="input"]');
    const volumeLabel = volumeWrapper.querySelector('[data-app="input-amount"]');

    const visualiser = document.querySelector('[data-app="visualiser"]');


    /**
     * Global values
     */
    let IS_STREAMING = false;
    let CURRENT_DELAY = parseInt(delay.value) / 100;
    let CURRENT_VOLUME = parseInt(volume.value) / 100;

    const BUTTON_COPY_PLAY = 'Stop';
    const BUTTON_COPY_STOP = 'Start';

    const audioMotion = new AudioMotionAnalyzer(visualiser, {
        audioCtx: Pizzicato.context,
        radial: true,
        volume: 0,
        overlay: true,
        showPeaks: false,
        showScaleX: false,
        showScaleY: false,
        showBgColor: false,
        mode: 1,
        stereo: true,
        maxDecibles: 0,
        minDecibels: -85,
    });

    const options = {
        bgColor: 'transparent',
        dir: 'v',
        colorStops: [ '#ffffff', '#00d2ff']
    }
    
    audioMotion.registerGradient('custom-grad', options );
    audioMotion.setOptions({ gradient: 'custom-grad' });

    /**
     * Stream WAD
     */
    let streamInput = new Pizzicato.Sound({
        source: 'input',
        options: { volume: CURRENT_VOLUME }
    }, () => audioMotion.connectInput(streamInput.getInputNode()));

    /**
     * Attach delay node to input
     */
    var delayEffect = new Pizzicato.Effects.Delay({
        feedback: 0,
        time: CURRENT_DELAY,
        mix: 1
    });

    streamInput.addEffect(delayEffect);

    let stream;


    /**
     * Event Listeners
     */

    button.addEventListener('click', (e) => {
        if(!IS_STREAMING) startStream();
            else stopStream();
    });

    delay.addEventListener('input', (e) => onDelayChange(e));
    volume.addEventListener('input', (e) => onVolumeChange(e));


    /**
     * On delay change, update input node
     * @param {Object} e 
     */
    function onDelayChange(e) {

        let value = parseInt(e.currentTarget.value) / 100;
        
        CURRENT_DELAY = value;        
        streamInput.effects[0].time = CURRENT_DELAY;

        delayLabel.innerHTML = value;

    }


    /**
     * On volume change, update input node
     * @param {Object} e 
     */
    function onVolumeChange(e) {

        let value = parseInt(e.target.value) / 100;
        let roundedValue = Math.round(value * 10) / 10;
        
        CURRENT_VOLUME = roundedValue;
        streamInput.volume = CURRENT_VOLUME;
        
        volumeLabel.innerHTML = roundedValue * 100 + '%';

    }


    /**
     * Start mofucking stream
     */
    function startStream() {

        IS_STREAMING = true;
        streamInput.play();

        // Maybe sort out some additional button style toggles
        button.innerHTML = BUTTON_COPY_PLAY;

    }

    function stopStream() {

        IS_STREAMING = false;
        streamInput.stop();

        button.innerHTML = BUTTON_COPY_STOP;

    }

});
